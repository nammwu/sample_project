/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package com.example.sample_project;

import java.util.ArrayList;
import java.util.Locale;

import org.apache.cordova.CordovaActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

/*
 * 1. 갤럭시노트2(4.4.2-킷캣)에서 모든기능 정상작동
 * 2. 갤럭시플레이어(4.0.4-아이스크림샌드위치)에서 모든기능 정상작동
 * 
 * 3. 내컴퓨터 서버로 놓고 로컬말고 네트워크로 웹뷰써보기
 * 4. JS부분 펑션 정리, image, CSS사용해서 UI꾸미기
 * 5. 디펜시브 코딩하기
 * 
 * */
public class MainActivity extends CordovaActivity implements OnInitListener
{
	private String userInputStr="";
	private String userInputSpeech="";

	private WebView webView;

	private Handler mHandler = new Handler();
	private TextToSpeech mTTS;

	@SuppressLint("JavascriptInterface") @Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		// Set by <content src="index.html" /> in config.xml
		//loadUrl(launchUrl);
		//loadUrl("file:///android_asset/www/hello.html");

		setContentView(R.layout.activity_main);

		webView = (WebView)findViewById(R.id.webview);

		//xml내 웹뷰세팅
		WebSettings webSettings = webView.getSettings();
		webSettings.setSaveFormData(false);
		webSettings.setJavaScriptEnabled(true);
		webSettings.setSupportZoom(false);

		webView.setWebChromeClient(new WebBrowserClient());
		webView.addJavascriptInterface(new JavaScriptMethods(), "jsmethods");
		webView.loadUrl(launchUrl);

	}


	////////////	////////////	////////////	////////////	////////////
	/*TTS사용위해 액티비티 생성 및 인텐트 사용 불필요*/
	@SuppressWarnings("depreation")
	@Override
	public void onInit(int status) {
		// TODO Auto-generated method stub
		if(status == TextToSpeech.SUCCESS){
			if(mTTS.getLanguage()!=Locale.ENGLISH){
				mTTS.setLanguage(Locale.ENGLISH);
			}
			speakOut();
		}
	}

	@Override
	public void onDestroy(){
		super.onDestroy();
		if(mTTS!=null){
			mTTS.shutdown();
		}
	}

	public void speakOut(){
		mTTS.speak(userInputStr, TextToSpeech.QUEUE_FLUSH, null);
	}
	/*여기까지*/
	////////////	////////////	////////////	////////////	////////////




	/*TTS시작 */
	private void ttsStart(String input_str){
		userInputStr=input_str;
		mTTS = new TextToSpeech(this,this);
	}


	/*STT시작*/
	private void speechRecogStart(){

		Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		i.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getPackageName());
		//i.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "ko-KR");
		i.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "en-US");

		SpeechRecognizer mRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
		mRecognizer.setRecognitionListener(listener);
		mRecognizer.startListening(i);
		Toast.makeText(getApplicationContext(), "Speak..", Toast.LENGTH_SHORT).show();

	}

	/*JS인터페이스 클래스 정의 */
	final class JavaScriptMethods{
		public JavaScriptMethods(){

		}

		@JavascriptInterface//웹뷰에서 확인버튼 클릭시 호출
		public void okEvent(String input_str){
			ttsStart(input_str);
			Toast.makeText(getApplicationContext(), "'이제 음성인식을 버튼을 누르세요'", Toast.LENGTH_SHORT).show();
		}

		@JavascriptInterface//웹뷰에서 음성인식버튼 클릭시 호출
		public void speech_recog_Event(){

			mHandler.post(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					speechRecogStart();
				}
			});
		}


	}

	/*WebBrowserClient클래스 정의*/
	final class WebBrowserClient extends WebChromeClient{
		public boolean onJsAlert(WebView view, String url, String message, JsResult result){
			result.confirm();

			return true;
		}
	}

	/*RecognitionListener 정의*/
	private RecognitionListener listener = new RecognitionListener() {
		@Override
		public void onRmsChanged(float rmsdB) {
			// TODO Auto-generated method stub
		}
		@Override
		public void onResults(Bundle results) {
			// TODO Auto-generated method stub
			String key = "";
			key = SpeechRecognizer.RESULTS_RECOGNITION;
			ArrayList<String> mResult = results.getStringArrayList(key);
			String[] rs = new String[mResult.size()];
			mResult.toArray(rs);
			//tv.setText("->"+rs[0]);
			userInputSpeech= rs[0];
			Toast.makeText(getApplicationContext(), userInputSpeech, Toast.LENGTH_SHORT).show();
			webView.loadUrl("javascript:compareString('" + userInputSpeech + "')");
		}
		@Override
		public void onReadyForSpeech(Bundle params) {
			// TODO Auto-generated method stub
			//setContentView(R.layout.activity_main);
			//Toast.makeText(getApplicationContext(), "aaa", Toast.LENGTH_SHORT).show();
		}
		@Override
		public void onPartialResults(Bundle partialResults) {
			// TODO Auto-generated method stub
		}
		@Override
		public void onEvent(int eventType, Bundle params) {
			// TODO Auto-generated method stub
		}
		@Override
		public void onError(int error) {
			// TODO Auto-generated method stub
		}
		@Override
		public void onEndOfSpeech() {
			// TODO Auto-generated method stub
			//Toast.makeText(getApplicationContext(), "aaa", Toast.LENGTH_SHORT).show();
		}
		@Override
		public void onBufferReceived(byte[] buffer) {
			// TODO Auto-generated method stub
		}
		@Override
		public void onBeginningOfSpeech() {
			// TODO Auto-generated method stub
		}
	};

}
