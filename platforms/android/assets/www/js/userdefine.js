    document.getElementById("ok_btn").addEventListener("click", function(event){
        window.jsmethods.okEvent(document.getElementById("input_field").value);
    });

    document.getElementById("speech_recog_btn").addEventListener("click", function(event){
        window.jsmethods.speech_recog_Event();
    });

    function show_correct(){
      document.getElementById("bclass_correct").style.background="#34862E";
  }
  function show_incorrect(){
      document.getElementById("bclass_incorrect").style.background="#F15F5F";
  }
  function hide(){
      //document.getElementById("blink1").style.visibility = "hidden";
      document.getElementById("bclass_correct").style.background="#E8FFE2";
      document.getElementById("bclass_incorrect").style.background="#FFEAEA";
  }

  function compareString(userInputSpeech){
    if(document.getElementById("input_field").value==userInputSpeech){
        for(var i=450; i<2250; i+=450){
            setTimeout("show_correct()",i);
            setTimeout("hide()",i+225);
        }
    }
    else{
        for(var i=450; i<2250; i+=450){
            setTimeout("show_incorrect()",i);
            setTimeout("hide()",i+225);
        }
    }
}